'use strict';

import 'bootstrap.native';
import React from 'react';
import ReactDOM from 'react-dom';

const msg = 'Hello World';
const element = <div class="row"><div class="col-sm-6">{msg}</div></div>;

ReactDOM.render(
  element,
  document.getElementById('main')
);
